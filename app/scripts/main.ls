React     = require 'react'
Redux     = require 'redux'
ReactDOM  = react-dom = require 'react-dom'
Bootstrap = require 'react-bootstrap'

{ Button, Input } = Bootstrap

Header = React.createClass do
  render: ->
    ``(
      <div className='header'>
        Chip Analyzer
      </div>
    );``

SearchBar = React.createClass do
  render: ->
    ``(
      <div className='searchBar'>
        <Input
          className='searchBar_input'
          type='text'
          label='Stock Number'
        />
      </div>
    );``

DataTable = React.createClass do
  render: ->
    ``(
      <div className='searchBar'>
        DataTable
      </div>
    );``

ChipAnalyzer = React.createClass do
  render: ->
    ``(
      <div>
        <Header/>
        <SearchBar/>
        <DataTable/>
      </div>
    );``

``
ReactDOM.render(
  <ChipAnalyzer/>,
  document.getElementById('container')
);
``
